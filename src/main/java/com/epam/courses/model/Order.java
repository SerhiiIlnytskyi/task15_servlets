package com.epam.courses.model;

import java.util.HashSet;
import java.util.Set;

public class Order {

  private static int unique = 1;
  private long id;
  private OrderState state;
  private String client;
  private String address;

  Set<Pizza> pizzas = new HashSet<>();

  public Order(String client, String address, Set<Pizza> pizzas) {
    this.client = client;
    this.address = address;
    this.pizzas = pizzas;
    id = unique;
    unique++;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Set<Pizza> getPizzas() {
    return pizzas;
  }

  public void setPizzas(Set<Pizza> pizzas) {
    this.pizzas = pizzas;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public OrderState getState() {
    return state;
  }

  public void setState(OrderState state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "Order{" +
        "id=" + id +
        ", state=" + state +
        ", client='" + client + '\'' +
        ", address='" + address + '\'' +
        ", pizzas=" + pizzas +
        '}';
  }
}
