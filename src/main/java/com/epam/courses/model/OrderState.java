package com.epam.courses.model;

public enum OrderState {
  RECIVED, COOKING, SENT, DELIVERED
}
