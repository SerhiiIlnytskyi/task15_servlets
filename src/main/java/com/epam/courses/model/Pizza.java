package com.epam.courses.model;

public class Pizza {

  private static int unique = 1;
  private int id;
  private String pizzaName;

  public Pizza(String pizzaName) {
    this.pizzaName = pizzaName;
    id = unique;
    unique++;
  }

  public String getPizzaName() {
    return pizzaName;
  }

  public void setPizzaName(String pizzaName) {
    this.pizzaName = pizzaName;
  }

  @Override
  public String toString() {
    return "Pizza{" +
        "id=" + id +
        ", pizzaName='" + pizzaName + '\'' +
        '}';
  }
}
