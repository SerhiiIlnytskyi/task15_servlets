package com.epam.courses.controller;

import com.epam.courses.model.Order;
import com.epam.courses.model.Pizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {

  private static Logger logger = LogManager.getLogger(OrderServlet.class);
  private static Map<Long, Order> orders = new HashMap<>();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    PrintWriter out = resp.getWriter();
    resp.setContentType("text/html");
    out.println("<html><head><body>");
    out.println("<h2>Orders:</h2>");
    for (Entry order : orders.entrySet()) {
      out.println("<p>" + order.getKey() + ". " + order.getValue() + "</p>");
    }
    out.println("<form action='orders' method='POST'>\n"
        + "  Client name: <input type='text' name='order_client'>\n"
        + "  Client address: <input type='text' name='order_address'>\n"
        + "  Pizza name : <input type='text' name='order_pizza'>\n"
        + "  <button type='submit'>Order pizza</button>\n"
        + "</form>");
    out.println("<form>\n"
        + "  <h1><p>Delete order:</p></h1>\n"
        + "  <p> Order id: <input type='text' name='order_id'>\n"
        + "    <input type='button' onclick='remove(this.form.order_id.value)' name='ok' value='Delete Order'/>\n"
        + "  </p>\n"
        + "  </form>");
    out.println("<script type='text/javascript'>\n"
        + "  function remove(id) { fetch('orders/' + id, {method: 'DELETE'}); }\n"
        + "</script>");
    out.println("<p> <a href='/orders'>REFRESH PAGE</a> </p>");
    out.println("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String orderClientName = req.getParameter("order_client");
    String orderClientAddress = req.getParameter("order_address");
    String orderPizza = req.getParameter("order_pizza");
    Set<Pizza> pizzas = new HashSet<>();
    pizzas.add(new Pizza(orderPizza));
    Order order = new Order(orderClientName, orderClientAddress, pizzas);
    orders.put(order.getId(), order);
    doGet(req, resp);
    super.doPost(req, resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    super.doDelete(req, resp);
    String id = req.getRequestURI();
    id = id.replace("/servlets/orders/", "");
    orders.remove(id);
  }

  @Override
  public void destroy() {
    logger.info("servlet closed");
    super.destroy();
  }

  @Override
  public void init() throws ServletException {
    logger.info("init servlet");
    super.init();
  }
}
